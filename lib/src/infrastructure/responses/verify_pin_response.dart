import 'dart:convert';

import 'package:takebook_quiz/src/infrastructure/responses/response.dart';

class VerifyPinResponse implements ApiResponse {
  final String craetedAt;
  final String token;
  final String message;
  final bool success;
  VerifyPinResponse(this.craetedAt, this.token, this.message, this.success);

  Map<String, dynamic> toMap() {
    return {
      'craetedAt': craetedAt,
      'token': token,
      'message': message,
      'success': success,
    };
  }

  factory VerifyPinResponse.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return VerifyPinResponse(
      map['craetedAt'],
      map['token'],
      map['responseMessage'],
      map['responseCode'] == "SUCCESS",
    );
  }

  String toJson() => json.encode(toMap());

  factory VerifyPinResponse.fromJson(String source) =>
      VerifyPinResponse.fromMap(json.decode(source));
}
