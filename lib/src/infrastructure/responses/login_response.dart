import 'dart:convert';

import 'package:takebook_quiz/src/infrastructure/responses/response.dart';

class LoginResponse implements ApiResponse {
  final String message;
  final bool success;
  LoginResponse(this.message, this.success);

  Map<String, dynamic> toMap() {
    return {
      'message': message,
      'success': success,
    };
  }

  factory LoginResponse.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return LoginResponse(
      map['responseMessage'],
      map['responseCode'] == "SUCCESS",
    );
  }

  String toJson() => json.encode(toMap());

  factory LoginResponse.fromJson(String source) =>
      LoginResponse.fromMap(json.decode(source));
}
