import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:takebook_quiz/src/core/consts/consts.dart';
import 'package:takebook_quiz/src/domain/entities/verification_code.dart';
import 'package:takebook_quiz/src/domain/entities/user_credintials.dart';
import 'package:takebook_quiz/src/domain/facades/remote_data_source.dart';
import 'package:takebook_quiz/src/infrastructure/exceptions/no_internet_exception.dart';
import 'package:takebook_quiz/src/infrastructure/exceptions/parse_exception.dart';
import 'package:takebook_quiz/src/infrastructure/exceptions/server_exception.dart';
import 'package:takebook_quiz/src/infrastructure/exceptions/unknown_exception.dart';
import 'package:takebook_quiz/src/infrastructure/responses/login_response.dart';
import 'package:takebook_quiz/src/infrastructure/responses/verify_pin_response.dart';

class RemoteDataSource extends IRemoteDataSource {
  final Client _client;
  RemoteDataSource() : this._client = Client();

  @override
  Future<LoginResponse> loginUser({UserCredintials credintials}) async {
    final Map<String, String> body = {
      PHONE_NUMBER_KEY: credintials.phonenumber,
    };
    final Map<String, String> headers = {
      API_KEY_KEY: API_KEY,
    };

    final Uri url = Uri.https(
      REMOTE_API_HOST,
      REMOTE_API_LOGIN_PATH,
    );
    Response response;

    try {
      response = await _client.post(
        url,
        body: json.encode(body),
        headers: headers,
      );
    } on SocketException {
      throw NoInternetException();
    } catch (_) {
      throw UnknownException();
    }

    return _responseProccessor<LoginResponse>(
      response,
      (data) => LoginResponse.fromJson(data),
    );
  }

  @override
  Future<VerifyPinResponse> postUserPin(
      {VerificationCode verificationCode}) async {
    final Map<String, String> body = {
      PIN_KEY: verificationCode.pin,
      PHONE_NUMBER_KEY: verificationCode.phonenumber,
    };
    final Map<String, String> headers = {
      API_KEY_KEY: API_KEY,
    };
    final Uri url = Uri.https(
      REMOTE_API_HOST,
      REMOTE_API_VERIFY_PIN_PATH,
    );
    Response response;

    try {
      response = await _client.post(
        url,
        body: json.encode(body),
        headers: headers,
      );
      print(response.body);
      print(url);
      print(headers);
      print(body);
    } on SocketException {
      throw NoInternetException();
    } catch (_) {
      throw UnknownException();
    }

    return _responseProccessor<VerifyPinResponse>(
      response,
      (data) => VerifyPinResponse.fromJson(data),
    );
  }

  T _responseProccessor<T>(Response response, parser) {
    if (response.statusCode == 500) {
      throw ServerException();
    } else {
      try {
        return parser(response.body);
      } catch (_) {
        throw ParseException();
      }
    }
  }
}
