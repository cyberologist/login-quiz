import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:takebook_quiz/src/core/consts/consts.dart';
import 'package:takebook_quiz/src/domain/entities/auth_info.dart';
import 'package:dartz/dartz.dart';
import 'package:takebook_quiz/src/domain/facades/local_data_source.dart';

class LocalDataSource extends ILocalDataSource {
  SharedPreferences _prefrencesInstance;

  @override
  Future<AuthInfo> getAuthInfo() async {
    await _insureInitialized();

    return AuthInfo(
      _prefrencesInstance.getString(CREATED_AT_KEY),
      _prefrencesInstance.getString(TOKEN_KEY),
    );
  }

  @override
  Future<Unit> storeAuthInfo({
    @required AuthInfo authInfo,
  }) async {
    await _insureInitialized();
    await _prefrencesInstance.setString(CREATED_AT_KEY, authInfo.createdAt);
    await _prefrencesInstance.setString(TOKEN_KEY, authInfo.token);
    return unit;
  }

  _insureInitialized() async {
    if (_prefrencesInstance == null) {
      _prefrencesInstance = await SharedPreferences.getInstance();
    }
  }
}
