import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:takebook_quiz/src/domain/entities/auth_info.dart';
import 'package:takebook_quiz/src/domain/entities/verification_code.dart';
import 'package:takebook_quiz/src/domain/entities/user_credintials.dart';
import 'package:takebook_quiz/src/domain/facades/local_data_source.dart';
import 'package:takebook_quiz/src/domain/failures/failure.dart';
import 'package:takebook_quiz/src/domain/facades/remote_data_source.dart';
import 'package:takebook_quiz/src/domain/facades/repository.dart';
import 'package:takebook_quiz/src/domain/failures/login_failures.dart';
import 'package:takebook_quiz/src/domain/failures/network_failure.dart';
import 'package:takebook_quiz/src/domain/failures/server_failure.dart';
import 'package:takebook_quiz/src/domain/failures/unknwon_failure.dart';
import 'package:takebook_quiz/src/infrastructure/exceptions/no_internet_exception.dart';
import 'package:takebook_quiz/src/infrastructure/exceptions/parse_exception.dart';
import 'package:takebook_quiz/src/infrastructure/exceptions/server_exception.dart';
import 'package:takebook_quiz/src/infrastructure/exceptions/unknown_exception.dart';
import 'package:takebook_quiz/src/infrastructure/responses/login_response.dart';
import 'package:takebook_quiz/src/infrastructure/responses/verify_pin_response.dart';

class Repository implements IRepository {
  final IRemoteDataSource _remoteDataSource;
  final ILocalDataSource _localDataSource;

  Repository(this._remoteDataSource, this._localDataSource);

  @override
  Future<Either<Failure, String>> loginUser(
      {@required UserCredintials userCredintials}) async {
    try {
      LoginResponse loginResponse = await _remoteDataSource.loginUser(
        credintials: userCredintials,
      );

      if (loginResponse.success) {
        return Right(loginResponse.message);
      } else {
        return Left(InvalidPhonenumberFailure(loginResponse.message));
      }
    } on NoInternetException {
      return Left(NetworkFailure());
    } on UnknownException {
      return Left(UnknwonFailure());
    } on ParseException {
      return Left(ServerFailure());
    } on ServerException {
      return Left(ServerFailure());
    } catch (_) {
      return Left(UnknwonFailure());
    }
  }

  @override
  Future<Either<Failure, String>> postUserPin({
    @required VerificationCode verificationCode,
  }) async {
    try {
      VerifyPinResponse verifyPinResponse = await _remoteDataSource.postUserPin(
        verificationCode: verificationCode,
      );
      if (verifyPinResponse.success) {
        final AuthInfo authInfo =
            AuthInfo(verifyPinResponse.craetedAt, verifyPinResponse.token);

        // cache the token and createdAt for later use
        await _localDataSource.storeAuthInfo(authInfo: authInfo);

        return Right(verifyPinResponse.message);
      } else {
        return Left(InvalidPinFailure(verifyPinResponse.message));
      }
    } on NoInternetException {
      return Left(NetworkFailure());
    } on UnknownException {
      return Left(UnknwonFailure());
    } on ParseException {
      return Left(ServerFailure());
    } on ServerException {
      return Left(ServerFailure());
    } catch (_) {
      return Left(UnknwonFailure());
    }
  }
}
