import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:takebook_quiz/src/domain/failures/failure.dart';
import 'package:takebook_quiz/src/domain/entities/user_credintials.dart';
import 'package:takebook_quiz/src/domain/entities/verification_code.dart';

abstract class IRepository {
  // failure ot success message
  Future<Either<Failure, String>> loginUser({
    @required UserCredintials userCredintials,
  });

  // failure ot success message
  Future<Either<Failure, String>> postUserPin({
    @required VerificationCode verificationCode,
  });
}
