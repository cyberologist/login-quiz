import 'package:flutter/foundation.dart';
import 'package:takebook_quiz/src/domain/entities/user_credintials.dart';
import 'package:takebook_quiz/src/domain/entities/verification_code.dart';
import 'package:takebook_quiz/src/infrastructure/responses/login_response.dart';
import 'package:takebook_quiz/src/infrastructure/responses/verify_pin_response.dart';

abstract class IRemoteDataSource {
  Future<LoginResponse> loginUser({
    @required UserCredintials credintials,
  });
  Future<VerifyPinResponse> postUserPin({
    @required VerificationCode verificationCode,
  });
}
