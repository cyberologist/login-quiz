import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:takebook_quiz/src/domain/entities/auth_info.dart';

abstract class ILocalDataSource {
  Future<Unit> storeAuthInfo({
    @required AuthInfo authInfo,
  });
  Future<AuthInfo> getAuthInfo();
}
