class AuthInfo {
  final String createdAt;
  final String token;

  AuthInfo(this.createdAt, this.token);
}
