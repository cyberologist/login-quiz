import 'package:flutter/foundation.dart';

class VerificationCode {
  final String pin;
  final String phonenumber;

  VerificationCode({
    @required this.pin,
    @required this.phonenumber,
  });
}
