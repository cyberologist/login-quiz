import 'package:takebook_quiz/src/core/consts/messages.dart';
import 'package:takebook_quiz/src/domain/failures/failure.dart';

class UnknwonFailure extends Failure {
  UnknwonFailure() : super(UNKNOWN_ERROR_MESSAGE);
}
