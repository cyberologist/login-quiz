import 'package:takebook_quiz/src/domain/failures/failure.dart';

class InvalidPinFailure extends Failure {
  InvalidPinFailure(String message) : super(message);
}

class InvalidPhonenumberFailure extends Failure {
  InvalidPhonenumberFailure(String message) : super(message);
}
