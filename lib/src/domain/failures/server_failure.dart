import 'package:takebook_quiz/src/core/consts/messages.dart';
import 'package:takebook_quiz/src/domain/failures/failure.dart';

class ServerFailure extends Failure {
  ServerFailure() : super(SERVER_ERROR_MESSAGE);
}
