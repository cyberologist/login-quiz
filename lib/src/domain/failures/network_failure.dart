import 'package:takebook_quiz/src/core/consts/messages.dart';
import 'package:takebook_quiz/src/domain/failures/failure.dart';

class NetworkFailure extends Failure {
  NetworkFailure() : super(NETWORK_ERROR_MESSAGE);
}
