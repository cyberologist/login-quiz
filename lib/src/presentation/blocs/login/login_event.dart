part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class LoginPressed extends LoginEvent {
  final String phonenumber;

  LoginPressed({
    @required this.phonenumber,
  });
}
