part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginLoading extends LoginState {}

class LoginError extends LoginState {
  final String message;

  LoginError({@required this.message});
}

class LoginSuccess extends LoginState {
  final String message;
  final String phonenumber;

  LoginSuccess({
    @required this.message,
    @required this.phonenumber,
  });
}
