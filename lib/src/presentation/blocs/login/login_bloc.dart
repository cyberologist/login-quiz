import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:takebook_quiz/src/core/extentions/dartz_extension.dart';
import 'package:takebook_quiz/src/application/usecases/login.dart';
import 'package:takebook_quiz/src/domain/failures/failure.dart';
import 'package:takebook_quiz/src/domain/entities/user_credintials.dart';
import 'package:takebook_quiz/src/presentation/validators/phone_number.dart';
part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({
    @required this.loginUser,
    @required this.validatePhoneNumber,
  }) : super(LoginInitial());
  final Login loginUser;
  final ValidatePhoneNumber validatePhoneNumber;

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is LoginPressed) {
      yield LoginLoading();

      // validating phone number locally
      final Either<String, Unit> validationError =
          validatePhoneNumber(event.phonenumber);

      bool phonenumberValid = validationError.isRight();
      if (!phonenumberValid) {
        yield LoginError(message: validationError.getLeft());
      } else {
        final UserCredintials userCredintials =
            UserCredintials(phonenumber: event.phonenumber);
        // loging in to the server
        final Either<Failure, String> loginResult =
            await loginUser(userCredintials);
        yield loginResult.fold(
          (failure) => LoginError(message: failure.message),
          (successMessage) => LoginSuccess(
            message: successMessage,
            phonenumber: userCredintials.phonenumber,
          ),
        );
      }
    }
  }
}
