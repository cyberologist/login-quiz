part of 'verification_bloc.dart';

@immutable
abstract class VerificationEvent {}

class VerificationCodeSibmitted extends VerificationEvent {
  final String verificationCode;
  final String phonenumber;

  VerificationCodeSibmitted({
    @required this.verificationCode,
    @required this.phonenumber,
  });
}
