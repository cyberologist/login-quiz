import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:takebook_quiz/src/application/usecases/verifiy_pin.dart';
import 'package:takebook_quiz/src/domain/failures/failure.dart';
import 'package:takebook_quiz/src/core/extentions/dartz_extension.dart';
import 'package:takebook_quiz/src/domain/entities/verification_code.dart';
import 'package:takebook_quiz/src/presentation/validators/validate_pin.dart';

part 'verification_event.dart';
part 'verification_state.dart';

class VerificationBloc extends Bloc<VerificationEvent, VerificationState> {
  final ValidatePin validatePin;
  final VerifyPin verifyPin;
  VerificationBloc({
    @required this.validatePin,
    @required this.verifyPin,
  }) : super(VerificationInitial());

  @override
  Stream<VerificationState> mapEventToState(
    VerificationEvent event,
  ) async* {
    if (event is VerificationCodeSibmitted) {
      yield VerificationCodeSubmitting();

      //validating pin locally
      final Either<String, Unit> validationError =
          validatePin(event.verificationCode);

      bool pinValid = validationError.isRight();
      if (!pinValid) {
        yield VerificationCodeIncorrect(message: validationError.getLeft());
      } else {
        final VerificationCode verificationCode = VerificationCode(
          pin: event.verificationCode,
          phonenumber: event.phonenumber,
        );
        // validate pin on the server
        final Either<Failure, String> loginResult =
            await verifyPin(verificationCode);

        yield loginResult.fold(
          (failure) => VerificationCodeIncorrect(message: failure.message),
          (successMessage) => VerificationCodeCorrect(message: successMessage),
        );
      }
    }
  }
}
