part of 'verification_bloc.dart';

@immutable
abstract class VerificationState {}

class VerificationInitial extends VerificationState {}

class VerificationCodeSubmitting extends VerificationState {}

class VerificationCodeCorrect extends VerificationState {
  final String message;

  VerificationCodeCorrect({@required this.message});
}

class VerificationCodeIncorrect extends VerificationState {
  final String message;

  VerificationCodeIncorrect({@required this.message});
}
