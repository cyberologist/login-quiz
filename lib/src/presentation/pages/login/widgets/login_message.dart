import 'package:flutter/material.dart';
import 'package:takebook_quiz/src/core/consts/messages.dart';

class LoginMessage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            WELCOME,
            style: TextStyle(
              fontSize: 32.0,
              color: Theme.of(context).primaryColor,
            ),
          ),
          Text(
            BACK,
            style: TextStyle(
              fontSize: 32.0,
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}
