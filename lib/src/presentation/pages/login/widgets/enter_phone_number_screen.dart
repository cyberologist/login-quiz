import 'package:flutter/material.dart';
import 'package:takebook_quiz/src/core/injection/injection_container.dart';
import 'package:takebook_quiz/src/presentation/blocs/login/login_bloc.dart';
import 'package:takebook_quiz/src/presentation/pages/login/widgets/login_message.dart';
import 'package:takebook_quiz/src/presentation/widgets/login_button.dart';

class EnterPhonenumberScreen extends StatefulWidget {
  @override
  _EnterPhonenumberScreenState createState() => _EnterPhonenumberScreenState();
}

class _EnterPhonenumberScreenState extends State<EnterPhonenumberScreen> {
  String phone;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          LoginMessage(),
          SizedBox(height: 30),
          buildPhoneField(),
          SizedBox(height: 25),
          LoginButton(onLoginSubmit: onLoginSubmit),
          SizedBox(height: 40),
        ],
      ),
    );
  }

  Padding buildPhoneField() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 32),
      child: Material(
        color: Colors.grey[100],
        elevation: 5.0,
        shadowColor: Colors.black38,
        borderRadius: BorderRadius.all(Radius.circular(30)),
        child: TextField(
          onChanged: onPhoneChange,
          cursorColor: Theme.of(context).primaryColor,
          keyboardType: TextInputType.phone,
          decoration: InputDecoration(
            hintText: "Phone",
            prefixIcon: Material(
              elevation: 0,
              borderRadius: BorderRadius.all(Radius.circular(30)),
              child: Icon(
                Icons.phone,
                color: Theme.of(context).primaryColorLight,
              ),
            ),
            border: InputBorder.none,
            contentPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 13),
          ),
        ),
      ),
    );
  }

  onPhoneChange(String value) {
    setState(() {
      phone = value;
    });
  }

  onLoginSubmit() {
    injectionContainer<LoginBloc>().add(LoginPressed(phonenumber: phone));
  }
}
