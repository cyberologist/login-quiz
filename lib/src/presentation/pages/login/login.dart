import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:takebook_quiz/src/presentation/blocs/login/login_bloc.dart';
import 'package:takebook_quiz/src/presentation/misc/snackbars.dart';
import 'package:takebook_quiz/src/core/injection/injection_container.dart';
import 'package:takebook_quiz/src/presentation/pages/login/widgets/enter_phone_number_screen.dart';
import 'package:takebook_quiz/src/presentation/pages/verify/verify.dart';
import 'package:takebook_quiz/src/presentation/widgets/loading_screen.dart';
import 'package:takebook_quiz/src/presentation/widgets/waves.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  // ignore: close_sinks
  LoginBloc bloc;
  bool canEnterPin = false;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = injectionContainer<LoginBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          buildBannerDecoration(context),
          BlocConsumer<LoginBloc, LoginState>(
            buildWhen: (previous, current) {
              return (current is LoginInitial ||
                  current is LoginLoading ||
                  current is LoginError);
            },
            builder: (BuildContext context, LoginState state) {
              if (state is LoginInitial || state is LoginError) {
                return EnterPhonenumberScreen();
              } else
                return LoadingScreen();
            },
            listener: (BuildContext context, LoginState state) {
              if (state is LoginError) {
                showErrorSnackBar(context, state.message);
              } else if (state is LoginSuccess) {
                showSuccessSnackBar(context, state.message);
                _navigateToVerifyCode(state.phonenumber);
              }
            },
            cubit: bloc,
          )
        ],
      ),
    );
  }

  Stack buildBannerDecoration(BuildContext context) {
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: WaveClipper3(),
          child: Container(
            child: Column(),
            width: double.infinity,
            height: 200,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Colors.white,
                  Theme.of(context).primaryColorLight,
                  Theme.of(context).primaryColor,
                ],
              ),
            ),
          ),
        ),
        ClipPath(
          clipper: WaveClipper1(),
          child: Container(
            child: Column(
              children: <Widget>[
                SizedBox(height: 150),
                Text(
                  "TakeBook",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 30,
                  ),
                ),
              ],
            ),
            width: double.infinity,
            height: 300,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Theme.of(context).primaryColorLight,
                  Theme.of(context).primaryColor,
                  Theme.of(context).primaryColorDark,
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _navigateToVerifyCode(String phonenumber) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return VerifyScreen(phonenumber: phonenumber);
        },
      ),
    );
  }
}
