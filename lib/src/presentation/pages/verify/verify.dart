import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:takebook_quiz/src/presentation/blocs/verification/verification_bloc.dart';
import 'package:takebook_quiz/src/presentation/misc/snackbars.dart';
import 'package:takebook_quiz/src/core/injection/injection_container.dart';
import 'package:takebook_quiz/src/presentation/pages/verify/widgets/enter_pin_screen.dart';
import 'package:takebook_quiz/src/presentation/widgets/loading_screen.dart';
import 'package:takebook_quiz/src/presentation/widgets/waves.dart';

class VerifyScreen extends StatefulWidget {
  final String phonenumber;

  const VerifyScreen({Key key, @required this.phonenumber}) : super(key: key);
  @override
  _VerifyScreenState createState() => _VerifyScreenState();
}

class _VerifyScreenState extends State<VerifyScreen> {
  // ignore: close_sinks
  VerificationBloc bloc;
  bool canEnterPin = false;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = injectionContainer<VerificationBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          buildBannerDecoration(context),
          BlocConsumer<VerificationBloc, VerificationState>(
            buildWhen: (previous, current) {
              return (current is VerificationInitial ||
                  current is VerificationCodeSubmitting ||
                  current is VerificationCodeIncorrect ||
                  current is VerificationCodeCorrect);
            },
            builder: (BuildContext context, VerificationState state) {
              if (state is VerificationInitial ||
                  state is VerificationCodeIncorrect ||
                  state is VerificationCodeCorrect) {
                return EnterPinScreen(phonenumber: widget.phonenumber);
              } else
                return LoadingScreen();
            },
            listener: (BuildContext context, VerificationState state) {
              if (state is VerificationCodeIncorrect) {
                showErrorSnackBar(context, state.message);
              } else if (state is VerificationCodeCorrect) {
                showSuccessSnackBar(context, state.message);
              }
            },
            cubit: bloc,
          )
        ],
      ),
    );
  }

  Stack buildBannerDecoration(BuildContext context) {
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: WaveClipper3(),
          child: Container(
            child: Column(),
            width: double.infinity,
            height: 200,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Colors.white,
                  Theme.of(context).primaryColorLight,
                  Theme.of(context).primaryColor,
                ],
              ),
            ),
          ),
        ),
        ClipPath(
          clipper: WaveClipper1(),
          child: Container(
            child: Column(
              children: <Widget>[
                SizedBox(height: 100),
                Text(
                  "TakeBook",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 30,
                  ),
                ),
              ],
            ),
            width: double.infinity,
            height: 300,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Theme.of(context).primaryColorLight,
                  Theme.of(context).primaryColor,
                  Theme.of(context).primaryColorDark,
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
