import 'package:flutter/material.dart';
import 'package:takebook_quiz/src/core/consts/messages.dart';

class VerifyMessage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            VeRIFICATION_PROMPT,
            style: TextStyle(
              fontSize: 14.0,
              color: Theme.of(context).primaryColor,
            ),
          ),
          SizedBox(height: 10),
          Text(
            ENTER_CODE_RECIVED,
            style: TextStyle(
              fontSize: 18.0,
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}
