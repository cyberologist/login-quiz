import 'package:flutter/material.dart';
import 'package:takebook_quiz/src/core/consts/messages.dart';
import 'package:takebook_quiz/src/core/injection/injection_container.dart';
import 'package:takebook_quiz/src/presentation/blocs/verification/verification_bloc.dart';
import 'package:takebook_quiz/src/presentation/pages/verify/widgets/verify_message.dart';
import 'package:takebook_quiz/src/presentation/widgets/login_button.dart';

class EnterPinScreen extends StatefulWidget {
  final String phonenumber;

  const EnterPinScreen({Key key, @required this.phonenumber}) : super(key: key);
  @override
  _EnterPinScreenState createState() => _EnterPinScreenState();
}

class _EnterPinScreenState extends State<EnterPinScreen> {
  String pin;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          VerifyMessage(),
          SizedBox(height: 30),
          buildPinField(),
          SizedBox(height: 25),
          LoginButton(onLoginSubmit: onPinSubmit),
          SizedBox(height: 40),
        ],
      ),
    );
  }

  Padding buildPinField() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 32),
      child: Material(
        color: Colors.grey[100],
        elevation: 5.0,
        shadowColor: Colors.black38,
        borderRadius: BorderRadius.all(Radius.circular(30)),
        child: TextField(
          onChanged: onPinChange,
          cursorColor: Theme.of(context).primaryColor,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            hintText: PIN_HINT,
            prefixIcon: Material(
              elevation: 0,
              borderRadius: BorderRadius.all(Radius.circular(30)),
              child: Icon(
                Icons.circle,
                color: Theme.of(context).primaryColorLight,
              ),
            ),
            border: InputBorder.none,
            contentPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 13),
          ),
        ),
      ),
    );
  }

  onPinChange(String value) {
    setState(() {
      pin = value;
    });
  }

  onPinSubmit() {
    injectionContainer<VerificationBloc>().add(
      VerificationCodeSibmitted(
        verificationCode: pin,
        phonenumber: widget.phonenumber,
      ),
    );
  }
}
