import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:takebook_quiz/src/core/consts/messages.dart';

showSuccessSnackBar(BuildContext context, String message) {
  final snackBar = SnackBar(
    content: Text(message),
    backgroundColor: Colors.greenAccent,
  );
  Scaffold.of(context).showSnackBar(snackBar);
}

showErrorSnackBar(BuildContext context, String message) {
  final snackBar = SnackBar(
    content: Text(message ?? UNKNOWN_ERROR_MESSAGE),
    backgroundColor: Colors.redAccent,
  );
  Scaffold.of(context).showSnackBar(snackBar);
}
