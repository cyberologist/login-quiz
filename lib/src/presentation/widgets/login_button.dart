import 'package:flutter/material.dart';
import 'package:takebook_quiz/src/core/consts/messages.dart';

class LoginButton extends StatelessWidget {
  final Function onLoginSubmit;

  const LoginButton({Key key, this.onLoginSubmit}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(50)),
        gradient: LinearGradient(
          colors: [
            Theme.of(context).primaryColorLight,
            Theme.of(context).primaryColor,
          ],
        ),
      ),
      child: FlatButton(
        padding: EdgeInsets.symmetric(horizontal: 162),
        child: Text(
          LOGIN_LABEL,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
          ),
        ),
        onPressed: onLoginSubmit,
      ),
    );
  }
}
