import 'package:dartz/dartz.dart';
import 'package:takebook_quiz/src/core/consts/messages.dart';
import 'package:takebook_quiz/src/presentation/validators/validator.dart';

class ValidatePhoneNumber extends Validator<String> {
  @override
  Either<String, Unit> call(String params) {
    if (params != null &&
        (params.length == 13 || params.length == 14) &&
        params[0] == '0' &&
        params.substring(0, 5) == '00963' &&
        params[5] == '9' &&
        _isNumeric(params)) {
      return Right(unit);
    } else {
      return Left(INVALID_PHONE_MESSAGE);
    }
  }

  bool _isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }
}
