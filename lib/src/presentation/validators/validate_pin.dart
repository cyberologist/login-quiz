import 'package:dartz/dartz.dart';
import 'package:takebook_quiz/src/core/consts/messages.dart';
import 'package:takebook_quiz/src/presentation/validators/validator.dart';

class ValidatePin extends Validator<String> {
  @override
  Either<String, Unit> call(String params) {
    if (params != null && params.length == 6 && _isNumeric(params)) {
      return Right(unit);
    } else {
      return Left(INVALID_PIN_MESSAGE);
    }
  }

  bool _isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }
}
