import 'package:dartz/dartz.dart';

abstract class Validator<T> {
  Either<T, Unit> call(T params);
}
