import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:takebook_quiz/src/application/core/usecase.dart';
import 'package:takebook_quiz/src/domain/failures/failure.dart';
import 'package:takebook_quiz/src/domain/entities/user_credintials.dart';
import 'package:takebook_quiz/src/domain/facades/repository.dart';

class Login extends Usecase<String, UserCredintials> {
  final IRepository repository;

  Login({
    @required this.repository,
  });
  @override
  Future<Either<Failure, String>> call(UserCredintials params) async {
    return await repository.loginUser(userCredintials: params);
  }
}
