import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:takebook_quiz/src/application/core/usecase.dart';
import 'package:takebook_quiz/src/domain/failures/failure.dart';
import 'package:takebook_quiz/src/domain/entities/verification_code.dart';
import 'package:takebook_quiz/src/domain/facades/repository.dart';

class VerifyPin extends Usecase<String, VerificationCode> {
  final IRepository repository;

  VerifyPin({
    @required this.repository,
  });
  @override
  Future<Either<Failure, String>> call(VerificationCode params) async {
    return await repository.postUserPin(verificationCode: params);
  }
}
