import 'package:dartz/dartz.dart';
import 'package:takebook_quiz/src/domain/failures/failure.dart';

abstract class Usecase<Type, Params> {
  Future<Either<Failure, Type>> call(Params params);
}

class NoParams {}
