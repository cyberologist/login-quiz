import 'package:get/get.dart';

class Messages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': {
          NETWORK_ERROR_MESSAGE: 'a network error occured , please try again',
          INVALID_PHONE_NUMBER_MESSAGE:
              'the phone number is invalid , make sure it is in the correct form',
          INCORRECT_PASSWORD_MESSAGE:
              'your username or passwrod not valid, please try again',
          SERVER_ERROR_MESSAGE:
              'an internal error occured , sorry we are trying to fix it',
          UNKNOWN_ERROR_MESSAGE: 'an unknown error occured , please contact us',
          LOADING: 'Loading..'
        },
        'ar_AE': {
          NETWORK_ERROR_MESSAGE:
              'لقد حدث خطأ بالشبكة,  الرجاء المحاولة مرة أخرى',
          INVALID_PHONE_NUMBER_MESSAGE:
              "رقم هاتفك غير صالح , تأكد انه بالصيغة المطلوبة",
          INCORRECT_PASSWORD_MESSAGE:
              'كلمة السر او اسم المستخدم غير صحيح , الرجاء المحاولة مرة اخرى',
          SERVER_ERROR_MESSAGE: 'لقد حدث خطأ داخلي ,نعتذر نحن نحاول اصلاحه',
          UNKNOWN_ERROR_MESSAGE: 'لقد حدث خطأ غير معروف , الرجاء التواصل معنا',
          LOADING: 'جاري التحميل..'
        },
      };
}

const APP_NAME = "TakeBook";
const NETWORK_ERROR_MESSAGE = 'Network Error';
const INCORRECT_PASSWORD_MESSAGE = 'username or password invalid';
const INVALID_PHONE_NUMBER_MESSAGE = 'phone number invalid';
const UNKNOWN_ERROR_MESSAGE = 'Unknown Error';
const INVALID_PIN_MESSAGE = 'Invalid PIN please correct it.';
const INVALID_PHONE_MESSAGE =
    'Invalid phone number please be sure to include the national code.';
const SERVER_ERROR_MESSAGE = 'Server Error';
const ERROR = 'error';
const LOADING = 'Loading..';
const PIN_HINT = 'enter the pin you recived';
const LOGIN_LABEL = "Login";
const ENTER_CODE_RECIVED = "please enter the code you recived";
const VeRIFICATION_PROMPT = "Only one step remainging";
const WELCOME = "Welcome";
const BACK = " back !";
