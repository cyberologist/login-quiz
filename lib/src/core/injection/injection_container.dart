import 'package:get_it/get_it.dart';
import 'package:takebook_quiz/src/application/usecases/login.dart';
import 'package:takebook_quiz/src/application/usecases/verifiy_pin.dart';
import 'package:takebook_quiz/src/domain/facades/local_data_source.dart';
import 'package:takebook_quiz/src/domain/facades/remote_data_source.dart';
import 'package:takebook_quiz/src/domain/facades/repository.dart';
import 'package:takebook_quiz/src/infrastructure/data/repositories/repository.dart';
import 'package:takebook_quiz/src/infrastructure/data/sources/local_data_source.dart';
import 'package:takebook_quiz/src/infrastructure/data/sources/remote_data_source.dart';
import 'package:takebook_quiz/src/presentation/blocs/login/login_bloc.dart';
import 'package:takebook_quiz/src/presentation/blocs/verification/verification_bloc.dart';
import 'package:takebook_quiz/src/presentation/validators/phone_number.dart';
import 'package:takebook_quiz/src/presentation/validators/validate_pin.dart';

final injectionContainer = GetIt.instance;
Future<void> init() async {
  //data sources
  injectionContainer.registerLazySingleton<IRemoteDataSource>(
    () => RemoteDataSource(),
  );
  injectionContainer
      .registerLazySingleton<ILocalDataSource>(() => LocalDataSource());
  injectionContainer.registerLazySingleton<IRepository>(
    () => Repository(
      injectionContainer(),
      injectionContainer(),
    ),
  );

  // usecases
  injectionContainer.registerLazySingleton<VerifyPin>(
    () => VerifyPin(
      repository: injectionContainer(),
    ),
  );
  injectionContainer.registerLazySingleton<Login>(
    () => Login(
      repository: injectionContainer(),
    ),
  );

  // blocs
  injectionContainer.registerLazySingleton<LoginBloc>(
    () => LoginBloc(
      loginUser: injectionContainer(),
      validatePhoneNumber: injectionContainer(),
    ),
  );
  injectionContainer.registerLazySingleton<VerificationBloc>(
    () => VerificationBloc(
      verifyPin: injectionContainer(),
      validatePin: injectionContainer(),
    ),
  );

  // misc
  injectionContainer.registerLazySingleton<ValidatePhoneNumber>(
    () => ValidatePhoneNumber(),
  );
  injectionContainer.registerLazySingleton<ValidatePin>(
    () => ValidatePin(),
  );
}
