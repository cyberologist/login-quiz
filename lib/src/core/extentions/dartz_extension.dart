import 'package:dartz/dartz.dart';

extension exLeft on Either {
  dynamic getLeft() {
    return this.fold((l) => l, (r) => throw "failure not found");
  }
}
