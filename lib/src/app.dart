import 'package:flutter/material.dart';
import 'package:takebook_quiz/src/presentation/pages/login/login.dart';
import 'package:takebook_quiz/src/presentation/pages/verify/verify.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginScreen(),
    );
  }
}
