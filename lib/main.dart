import 'package:flutter/material.dart';
import 'package:takebook_quiz/src/app.dart';
import 'package:takebook_quiz/src/core/injection/injection_container.dart'
    as IC;

void main() async {
  await IC.init();
  runApp(App());
}
